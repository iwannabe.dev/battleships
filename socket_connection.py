import socket
import orjson
from decouple import config


class SocketConnection:
    """
    Handles the socket connection and related operations for the client-server application.

    Attributes:
        host (str): The host address.
        port (int): The port number.
        buffer_size (int): The buffer size for receiving data.
        address_family (socket.AddressFamily): The address family for the socket.
        socket_type (socket.SocketKind): The socket type.
        socket_obj (socket.socket): The underlying socket object.
        connection (socket.socket): The connection object for the server socket.
        ip_address (str): The IP address of the connected client.
    """
    def __init__(self):
        self.host = config('HOST')
        self.port = config('PORT', cast=int)
        self.buffer_size = config('BUFFER', cast=int)
        self.address_family = getattr(socket, config('ADDRESS_FAMILY'))
        self.socket_type = getattr(socket, config('SOCKET_TYPE'))
        self.socket_obj = None
        self.connection = None
        self.ip_address = None

    def serialize(self, data):
        """
        Serializes the given data and encodes it using the specified encoding format.

        Args:
            data (dict): The data to be serialized and encoded.

        Returns:
            bytes: The serialized and encoded data.
        """
        return orjson.dumps(data)

    def deserialize(self, data):
        """
        Deserializes the given data using the specified encoding format.

        Args:
            data (bytes): The data to be deserialized.

        Returns:
            dict: The deserialized data.
        """
        return orjson.loads(data)

    def setup_client_socket(self):
        """Sets up the client socket connection."""
        self.socket_obj = socket.socket(self.address_family, self.socket_type)
        self.socket_obj.connect((self.host, self.port))

    def setup_server_socket(self):
        """Sets up the server socket connection and accepts a client connection."""
        self.socket_obj = socket.socket(self.address_family, self.socket_type)
        self.socket_obj.bind((self.host, self.port))
        self.socket_obj.listen()
        self.connection, self.ip_address = self.socket_obj.accept()

    def send(self, data):
        """
        Sends the given data through the socket connection.

        Args:
            data (dict): The data to be sent.
        """
        serialized_data = self.serialize(data)

        if self.connection:
            self.connection.sendall(serialized_data)
        else:
            self.socket_obj.sendall(serialized_data)

    def receive(self):
        """
        Receives data from the socket connection and deserializes it.

        Returns:
            dict: The received and deserialized data.
        """
        if self.connection:
            response = self.connection.recv(self.buffer_size)
        else:
            response = self.socket_obj.recv(self.buffer_size)

        response_json = self.deserialize(response)
        return response_json

    def disconnect(self):
        """Disconnects the socket connection."""
        self.socket_obj.close()
