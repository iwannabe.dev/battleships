from socket_connection import SocketConnection


class Client:
    """
    Represents a client in the client-server application.

    Attributes:
        connection (SocketConnection): The socket connection object.
        is_running (bool): Flag to indicate if the client is running.
    """
    def __init__(self, socket_conn: object):
        self.connection = socket_conn
        self.is_running = True

    def run(self):
        """
        Runs the client's main loop, including:
        - Setting up the client socket connection
        - Receiving and printing the initial server response
        - Continuously prompting the user for input, sending it to the server, and printing the server's response
        - Disconnecting the client when the loop is finished
        """
        self.connection.setup_client_socket()

        try:
            server_response = self.connection.receive()
            print(server_response)

            while self.is_running:
                print("> ", end="")
                user_command = input()
                self.connection.send(user_command)

                server_response = self.connection.receive()
                print(server_response)
        finally:
            self.connection.disconnect()


if __name__ == "__main__":
    socket_connection = SocketConnection()
    client = Client(socket_connection)
    client.run()
